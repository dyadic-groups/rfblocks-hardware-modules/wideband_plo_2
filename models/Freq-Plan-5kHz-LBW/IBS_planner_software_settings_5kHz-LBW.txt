ComboBox: PLL1SelectionBox.SelectedIndex = 0
ComboBox: R4MultiplierBox.SelectedIndex = 0
ComboBox: C4MultiplierBox.SelectedIndex = 2
ComboBox: R3MultiplierBox.SelectedIndex = 1
ComboBox: C3MultiplierBox.SelectedIndex = 1
ComboBox: R2MultiplierBox.SelectedIndex = 0
ComboBox: C2MultiplierBox.SelectedIndex = 0
ComboBox: C1MultiplierBox.SelectedIndex = 1
ComboBox: ModeSelectBox.SelectedIndex = 1
ComboBox: ChooseDeviceBox.SelectedIndex = 0
CheckBox: AdjustfvcoBox.Checked = False
CheckBox: OpenPlotsInExcelAfterExportingBox.Checked = False
CheckBox: AutostepThroughChannelsBox.Checked = False
CheckBox: OpeninExcelBox.Checked = False
CheckBox: ShortOpeninExcelBox.Checked = False
TextBox: AvailableFrefsBox.Text = 250\n253.125\n254.166667\n256.25\n258.333333\n259.375\n262.5\n265.625\n266.666667\n268.75\n270.833333\n271.875\n275\n278.125\n279.166667\n281.25\n283.333333\n284.375\n287.5\n290.625\n291.666667\n293.75\n295.833333\n296.875\n300\n303.125\n304.166667\n306.25\n308.333333\n309.375\n312.5\n316.666667\n320.833333\n325\n329.166667\n333.333333\n337.5\n341.666667\n345.833333\n350\n
NumericUpDown: CascadedMaximumOutputFreqBox.Value = 350
NumericUpDown: CascadedMinimumOutputFreqBox.Value = 250
NumericUpDown: CascadedRDividerBox.Value = 2
TextBox: referencesBox.Text = 150 MHz\n120 MHz\n100 MHz\n85.714286 MHz\n
NumericUpDown: MinMasterClockBox.Value = 0
NumericUpDown: MaxMasterClockBox.Value = 5000
NumericUpDown: p3spurBox.Value = -170
NumericUpDown: p1spurBox.Value = -50
NumericUpDown: MaxfrefBox.Value = 350
NumericUpDown: p5spurBox.Value = -170
NumericUpDown: p4spurBox.Value = -170
NumericUpDown: p2spurBox.Value = -68
NumericUpDown: NfracMinBox.Value = 20
NumericUpDown: NintminBox.Value = 16
NumericUpDown: fpdRangeMinBox.Value = 50
NumericUpDown: MaxIcpBox.Value = 2.5
NumericUpDown: fpdRangeMaxBox.Value = 150
NumericUpDown: fvcominBox.Value = 1500
NumericUpDown: IcpStepBox.Value = 20
RadioButton: LoopFilterManualRadio.Checked = True
NumericUpDown: C4Box.Value = 0
NumericUpDown: R4Box.Value = 0
NumericUpDown: R3Box.Value = 3.3
NumericUpDown: C3Box.Value = 3.3
NumericUpDown: R2Box.Value = 59
NumericUpDown: C2Box.Value = 1
NumericUpDown: C1Box.Value = 33
RadioButton: LoopFilterAutomaticRadio.Checked = False
NumericUpDown: LBWBox.Value = 100
NumericUpDown: PhaseMarginBox.Value = 70
TextBox: MoptionsBox.Text = 8\n10\n12\n14
NumericUpDown: SweepStartBox.Value = 50
NumericUpDown: SweepStopBox.Value = 3000.0
NumericUpDown: SweepStepsBox.Value = 29500
NumericUpDown: SweepStepSizeBox.Value = 0.1
CheckedListBox: RoptionsBox.CheckedIndex = 3
CheckedListBox: RoptionsBox.CheckedIndex = 4
CheckedListBox: RoptionsBox.CheckedIndex = 5
NumericUpDown: RefFreqBox.Value = 50
NumericUpDown: ClockNValueBox.Value = 24
NumericUpDown: SpurChartYaxisMinUpDownBox.Value = -105
NumericUpDown: SpurChartXaxisMinUpDownBox.Value = 50
NumericUpDown: SpurChartXaxisMaxUpDownBox.Value = 3000
NumericUpDown: SpurChartYaxisMaxUpDownBox.Value = 0
NumericUpDown: PhaseNoiseCarrierBox.Value = 1530.7
NumericUpDown: PhaseNoiseChartYaxisMinUpDownBox.Value = -160
NumericUpDown: PhaseNoiseChartXaxisMinUpDownBox.Value = 100
NumericUpDown: PhaseNoiseChartXaxisMaxUpDownBox.Value = 1000000000
NumericUpDown: PhaseNoiseChartYaxisMaxUpDownBox.Value = -60
NumericUpDown: FrefandFpdChartXaxisMinUpDownBox.Value = 50
NumericUpDown: FrefandFpdChartXaxisMaxUpDownBox.Value = 3000
NumericUpDown: RefChartYaxisMinUpDownBox.Value = 0
NumericUpDown: RefChartYaxisMaxUpDownBox.Value = 400
NumericUpDown: ChargePumpChartXaxisMinUpDownBox.Value = 50
NumericUpDown: ChargePumpChartXaxisMaxUpDownBox.Value = 3000
NumericUpDown: ChargePumpChartYaxisMinUpDownBox.Value = 0
NumericUpDown: ChargePumpChartYaxisMaxUpDownBox.Value = 2.6
NumericUpDown: NDividerChartXaxisMinUpDownBox.Value = 50
NumericUpDown: NDividerChartXaxisMaxUpDownBox.Value = 3000
NumericUpDown: NDividerChartYaxisMinUpDownBox.Value = 15
NumericUpDown: NDividerChartYaxisMaxUpDownBox.Value = 60
NumericUpDown: DividersChartXaxisMinUpDownBox.Value = 50
NumericUpDown: DividersChartXaxisMaxUpDownBox.Value = 3000
NumericUpDown: DividersChartYaxisMinUpDownBox.Value = 0
NumericUpDown: DividersChartYaxisMaxUpDownBox.Value = 15
