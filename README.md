# An HMC833 Frequency Synthesizer

## Features

- Fractional-N and integer modes.
- 24-bit step size: 3 Hz resolution typically.
- Exact frequency mode.
- Wide bandwidth: 25 to 6000 MHz.
- Good phase noise performance: -120 dBc/Hz at 10 kHz offset (1 GHz carrier).
- Dual outputs with flexible output amplifier configuration.

## Documentation

Full documentation for the synthesizer module is available at
[RF Blocks](https://rfblocks.org/boards/HMC833-Synth.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)


